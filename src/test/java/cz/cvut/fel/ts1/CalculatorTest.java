package cz.cvut.fel.ts1;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertThrows;
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CalculatorTest {
    private static Calculator calculator;


    @BeforeAll
    public static void setup() {
        System.out.println("Init calculator");
        calculator = new Calculator();
    }

    @Test
    @Order(1)
    public void addition_minus5_plus_minus2_returnsMinus7() {
        System.out.println("Test addition_minus5_plus_minus2_returnsMinus7");
        // ARRANGE
        int a = -5;
        int b = -2;
        int expected = -7;
        // ACT
        int result = calculator.addition(a, b);
        // ASSERT
        Assertions.assertEquals(expected, result);
    }

    @Test
    @Order(2)
    //@DisplayName("Scitam {a} + {b} = {c}")
    public void addition_5plus6_returns11() {
        System.out.println("Test addition_5plus6_returns11");
        // ARRANGE
        int a = 5;
        int b = 6;
        int expected = 11;
        // ACT
        int result = calculator.addition(a, b);
        // ASSERT
        Assertions.assertEquals(expected, result);
    }
    @Test
    @Order(4)
    public void subtract_9minus3_returns6() {
        System.out.println("Test subtract_9minus3_returns6");
        // ARRANGE
        int a = 9;
        int b = 3;
        int expected = 6;
        // ACT
        int result = calculator.subtract(a, b);
        // ASSERT
        Assertions.assertEquals(expected, result);
    }
    @Test
    @Order(3)
    public void subtract_Minus10_minus_5_returnsMinus15() {
        System.out.println("Test subtract_Minus10_minus_5_returnsMinus1");
        // ARRANGE
        int a = -10;
        int b = 5;
        int expected = -15;
        // ACT
        int result = calculator.subtract(a, b);
        // ASSERT
        Assertions.assertEquals(expected, result);


    }
    @Test
    @Order(5)
    public void multiply_5mupltiply5_returns25() {
        System.out.println("Test multiply_5mupltiply5_returns25");
        // ARRANGE
        int a = 5;
        int b = 5;
        int expected = 25;
        // ACT
        int result = calculator.multiply(a, b);
        // ASSERT
        Assertions.assertEquals(expected, result);
    }
    @Test
    @Order(6)
    public void multiply_8multiply0_returns0() {
        System.out.println("Test multiply_8multiply0_returns0");
        // ARRANGE
        int a = 8;
        int b = 0;
        int expected = 0;
        // ACT
        int result = calculator.multiply(a, b);
        // ASSERT
        Assertions.assertEquals(expected, result);
    }

    @Test
    @Order(8)
    public void divide_5divide0_returnsException() throws Exception {
        System.out.println("Test divide_5divide0_returnsException");
        // ARRANGE
        int a = 5;
        int b = 0;
        String expected = "java.lang.ArithmeticException";
        // ACT
        Exception result = assertThrows(Exception.class, () -> {
            calculator.divide(a, b);
        });
        // ASSERT
        Assertions.assertEquals(expected, result.toString());

    }
    @Test
    @Order(7)
    public void divide_16divide4_returns4() throws Exception {
        System.out.println("Test divide_16divide4_returns4");
        // ARRANGE
        int a = 16;
        int b = 4;
        int expected = 4;
        // ACT
        int result = calculator.divide(a, b);
        // ASSERT
        Assertions.assertEquals(expected, result);
    }
    @Test
    @Order(9)
    public void throwException_default_exceptionIsThrown(){
        System.out.println("Test throwException_default_exceptionIsThrown");
        Assertions.assertThrows(Exception.class, () -> {
            calculator.throwException();
        });
    }


    @AfterEach
    public void tearDown() {
        System.out.println("Tested method");
    }
}


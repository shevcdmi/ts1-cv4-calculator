package cz.cvut.fel.ts1;

import java.util.Scanner;

public class Calculator {

    public int addition(int a, int b) {
        return a + b;
    }

    public int subtract(int a, int b) {
        return a - b;
    }

    public int multiply(int a, int b) {
        return a * b;
    }

    public int divide(int a, int b) throws Exception {
        if (b == 0) {
            throw new ArithmeticException();
        }
        return a / b;
    }

    public void throwException() throws Exception {
        throw new Exception();
    }
}
